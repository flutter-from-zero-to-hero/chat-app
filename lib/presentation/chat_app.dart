import 'dart:html';

import 'package:flutter/material.dart';
import 'package:flutter_application_1/chat_domain/entities.dart';
import 'package:flutter_application_1/presentation/chat_page.dart';

class ChatApp extends StatelessWidget {
  const ChatApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SignInPage();
    //return ChatPage();
  }
}

class SignInPage extends StatefulWidget {
  SignInPage({Key key}) : super(key: key);

  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  TextEditingController _controller;

  @override
  void initState() {
    _controller = TextEditingController();
    _controller.text = "Gest";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("The Chat App")),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Icon(
              Icons.chat,
              size: 300,
              color: Colors.greenAccent[400],
            ),
            Text("Please Enter Your Chat Username:",
                style: TextStyle(fontSize: 30)),
            Container(
              width: MediaQuery.of(context).size.width * 0.7,
              child: TextField(
                controller: _controller,
              ),
            ),
            SizedBox(height: 30),
            Container(
              height: 50,
              width: 300,
              child: RaisedButton(
                color: Colors.blue,
                child: Text(
                  "Enter to the live chat >>>",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 20),
                ),
                onPressed: () {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (b) {
                    currentUsername = _controller.text;
                    return ChatPage();
                  }));
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
