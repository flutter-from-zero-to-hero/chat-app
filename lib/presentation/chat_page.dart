import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/chat_domain/entities.dart';

import 'message_widget.dart';

class ChatPage extends StatefulWidget {
  const ChatPage({Key key}) : super(key: key);

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  List<Message> chatMessages;

  @override
  void initState() {
    super.initState();
    chatMessages = [
      Message(msg: "", sender: SenderType.Title),
    ];
    onMessageReceived = (String message) {
      setState(() {
        chatMessages.add(Message(
          msg: message,
          sender: SenderType.Other,
        ));
      });
    };
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Welcome $currentUsername")),
      body: Column(
        children: [
          Expanded(child: ChatBubbles(chatMessages: chatMessages)),
          Sender(
            addMessage: (Message msg) {
              setState(() {
                chatMessages.add(msg);
              });
            },
            setMessage: (Message msg, MessageStatus newStatus) {
              setState(() {
                msg.sendMessageStatus = newStatus;
              });
            },
          ),
        ],
      ),
    );
  }
}

class ChatBubbles extends StatefulWidget {
  final List<Message> chatMessages;
  ChatBubbles({Key key, this.chatMessages}) : super(key: key);

  @override
  _ChatBubblesState createState() => _ChatBubblesState();
}

class _ChatBubblesState extends State<ChatBubbles> {
  ScrollController _scrollController;

  @override
  void initState() {
    _scrollController = ScrollController();

    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void scrollToBottom() {
    final bottomOffset = _scrollController.position.maxScrollExtent;
    _scrollController.animateTo(
      bottomOffset,
      duration: Duration(milliseconds: 200),
      curve: Curves.easeInOut,
    );
  }

  @override
  Widget build(BuildContext context) {
    if (widget.chatMessages.length > 1) {
      scrollToBottom();
    }
    return Container(
      color: Colors.amber[50],
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 30),
        child: ListView.builder(
          controller: _scrollController,
          itemCount: widget.chatMessages.length,
          itemBuilder: (_, index) {
            return ListTile(
                title: ChatMessage(
              message: widget.chatMessages[index],
            ));
          },
        ),
      ),
    );
  }
}

class Sender extends StatefulWidget {
  final void Function(Message msg) addMessage;
  final void Function(Message msg, MessageStatus newStatus) setMessage;
  Sender({Key key, this.addMessage, this.setMessage}) : super(key: key);

  @override
  _SenderState createState() => _SenderState();
}

class _SenderState extends State<Sender> {
  TextEditingController _controller;
  @override
  void initState() {
    _controller = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black87,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(),
            borderRadius: BorderRadius.all(Radius.circular(100)),
            color: Colors.white,
          ),
          child: Row(
            children: [
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.only(left: 30),
                child: TextField(controller: _controller),
              )),
              ClipOval(
                child: Material(
                  color: Colors.blue, // button color
                  child: InkWell(
                    splashColor: Colors.red, // inkwell color
                    child: SizedBox(
                        width: 56, height: 56, child: Icon(Icons.send)),
                    onTap: () {
                      Message message =
                          Message(msg: _controller.text, sender: SenderType.Me);
                      widget.addMessage(message);
                      usecases.sendMessage(message, () {
                        widget.setMessage(message, MessageStatus.Sent);
                      }, () {
                        widget.setMessage(message, MessageStatus.FailedToSent);
                      });
                    },
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
