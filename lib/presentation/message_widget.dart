import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/chat_domain/entities.dart';

class ChatMessage extends StatefulWidget {
  final Message message;
  const ChatMessage({Key key, this.message}) : super(key: key);

  @override
  _ChatMessageState createState() => _ChatMessageState();
}

class _ChatMessageState extends State<ChatMessage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
          crossAxisAlignment: widget.message.sender == SenderType.Me
              ? CrossAxisAlignment.end
              : CrossAxisAlignment.start,
          children: [
            if (widget.message.sender == SenderType.Me) ...{
              Bubble(
                margin: BubbleEdges.only(top: 10),
                alignment: Alignment.topRight,
                nip: BubbleNip.rightTop,
                color: Color.fromRGBO(225, 255, 199, 1.0),
                child: Column(
                  children: [
                    Text(widget.message.msg, textAlign: TextAlign.right),
                    Text(
                      widget.message.sendMessageStatus.toString().split('.')[1],
                      style: TextStyle(fontSize: 10),
                    ),
                  ],
                ),
              ),
            } else if (widget.message.sender == SenderType.Title) ...{
              Bubble(
                alignment: Alignment.center,
                color: Color.fromRGBO(212, 234, 244, 1.0),
                child: Text('TODAY',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 11.0)),
              ),
            } else ...{
              Bubble(
                margin: BubbleEdges.only(top: 10),
                alignment: Alignment.topLeft,
                nip: BubbleNip.leftTop,
                child: Column(
                  children: [
                    Text(widget.message.msg),
                  ],
                ),
              ),
            },
          ]),
    );
  }
}
