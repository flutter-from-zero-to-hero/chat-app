# Function - פונקציות
```dart
<return-value> <function-name>(<parametets>){
  <code>
}

//
// Example 1:
void main() {
  int currentHour = getHour();
  print('the current hour is $currentHour');

  String sentence = createSentence('barak', 'hello', ',welcome');
  print('$sentence');

}

// Example 2:
int getHour() {
  return 5;
}

// Example 3:
String createSentence(String name, String firstWord, String lastWord) {
  String result = "$firstWord $name $lastWord !";
  return result;
}

```

# Types - טיפוסים, משתנים ופרמטרים
Type               |        Explain             |          Values 
---                | ---                         | ---                  
bool               | אמת או שקר                   | false, true
int                | מספר שלם  integer             | -1000, -5, 0 , 1554524
double             | מספר עשרוני d                |  -1000, -5, 0 , 1554524, 1.5, 0.0003, -100.52
String             | משפט                          | 'vasalkj vsalnvasl', "kjf okewopne"
var                | המחשב ידע להסיק לבד את הTYPE | var x = 6; var y = "fsafs";


# null - מגדיר משתנה ללא ערך
```dart
  String userName = null;
  if (userName == null) {
    print("Please Enter Your Name:");
    userName = stdin.readLineSync(encoding: Encoding.getByName('utf-8'));
  }
  print("the username is: $userName");
```

# Conditions - הגדרת תנאים
1. `==` - מייצג שיוויון
2. `!=` - מייצג שוני
3. `<`
4. `>`
5. `=<`
6. `>=`
7. `&&` - וגם
8. `||` - או


# if-else - תנאים
### Only If
```dart
    // Only If
    if(x < y){
        // your code here
    }
```
### If and Else
```dart
    if(x < y){
        // your code here
    }else{
        // your code here
    }
```
### Two or more Ifs
```dart
    //Two or more Ifs
    if(x < 2){
        // your code here
        if(y > 0){
            // your code here
        }
    }else if( x >= 2 && x < 10){
        // your code here
    }else if( x >= 10 && x < 15){
        // your code here
    }else{
        // your code here
    }
```

# Loops - לולאות
```dart
//While
int x = 0;
while(x < y){   
    // your code here  
    x++; // x = x + 1;
}
   
//For
for(int x = 0; x < y; x++){   
    // your code here
}  
```

# Dart IO - קבלת מידע מהמשתמש
```dart
import 'dart:io';
import 'dart:convert';

//.... Your Code Here
print("Please Enter Your Name:");
String userName = stdin.readLineSync(encoding: Encoding.getByName('utf-8'));

//.... Your Code Here

// HELPERS:
int readNumberFromUser() {
  String choosenString =
      stdin.readLineSync(encoding: Encoding.getByName('utf-8'));
  int choosenInt = int.parse(choosenString);
  return choosenInt;
}

String readStringFromUser() {
  String choosenString =
      stdin.readLineSync(encoding: Encoding.getByName('utf-8'));
  return choosenString;
}
```

# Convert String to Int:
```dart
String numberAsString = "45";
//numberAsString = numberAsString + 1; // numberAsString == "451"

int number = int.parse(numberAsString);
//number = number + 1; // number == 46
```

# לפני תחילת עבודה launch.json  העתיקו את ההגדרות והדביקו בתוך
```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Dart & Flutter",
            "request": "launch",
            "type": "dart",
            "program": "main.dart",
            "internalConsoleOptions": "openOnSessionStart",
            "console": "terminal",
        }
        
    ]
}
```

# / and % - מודולו וחילוק
```dart
int x = 10;

int b = x % 3; // b == 1 
int c = x / 3; // C == 3

// x = 100
int b1 = x % 2; // b1 == 0
bool isEven = b1 == 0;

int c1 = x / 2; // c1 == 50

```
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------


# פרמטרים אופציונאליים ופרמטרים עם שמות
## Optional Parameters
נוכל להגדיר את הפונקציה באופן הבא,    
כך שהגיל זהו פרמטר חובה וכל השאר הם אופציונאליים
```dart
String calculateDietMenu(
  int age, [
  double height = 150,
  String gender,
  String favoriteFood,
]) {
    // your code here
}

calculateDietMenu(52); // OK
calculateDietMenu(52, 182); // OK
calculateDietMenu(52, 182, "Female"); // OK
calculateDietMenu(52, 182, "Female", "Candy"); // OK
calculateDietMenu(); // *WRONG*
```
## Optional and Named Parameters
כאן כל הפרמטרים הם אופציונאליים ואינם חובה, 
אך חייבים לתת להם שם כאשר קוראים לפונקציה
```dart
String easyCalculateDietMenu(
    {int age, 
     double height = 150, 
     String gender, 
     String favoriteFood,
    }) {
        // your code here
    }

calculateDietMenu(); // OK
calculateDietMenu(age: 28); //OK
calculateDietMenu(gender: "Male"); //OK
calculateDietMenu(age: 28, gender: "Male"); //OK
calculateDietMenu(28); // *WRONG*
```
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------


# Enum
נשתמש בטיפוס זה כדי להגדיר שמות מובנים לערכים שונים  שקבועים בתוכנה שלנו.
למשל, כאשר אנו יודעים איזה אופציות התפריט שלנו יכיל, נוכל להחזיק אותם בטיפוס מיוחד שנייצר כך:
```dart
enum PageName {
  welcomePage,
  register,
  login,
  editProfile,
  showProfile,
  resetPassword,
  errorUserExists,
  feedFacebookPage,
}

// you can use it like that:
PageName getCurrentPage(){
  return PageName.login;
}
PageName currentPage = getCurrentPage();
```

# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------


# Exceptions - Try{}  Catch(error){}
יכולת לזהות שגיאות לא ידועות, לתפוס אותן לפני שהמערכת קורסת
ולאחר מכן להצליח להציג הודעה אינפורמטיבית למשתמש ולהחזיר את המערכת לפעולה תקינה
להלן דוגמא לטיפול בשגיאות:

### יצירת שגיאה עם מסר מותאם אישית:
```dart
int readNumberFromUser() {
  int choosenInt = -1;
  try {
    // try to convert "24a" to integer(number).
    // Exception  - We cant do it ""24a" isnt a number!
    choosenInt = int.parse("24a");
  } catch (error) {
    // We catch the unknown exception and create new one,
    // with informative, castume message to the user:
    throw Exception("you entered invalid number, please enter integer.");
  }
  return choosenInt;
}
```

### תפיסת השגיאה והמשך תפעול רגיל ללא יצירת שום שגיאה נוספת
```dart
int readNumberFromUser() {
  int choosenInt = -1;
  try {
    // try to convert "24a" to integer(number).
    // Exception  - We cant do it ""24a" isnt a number!
    choosenInt = int.parse("24a");
  } catch (error) {
    print("Error occured, the system continue running")    
    print("Error is: $error")    
  }
  return choosenInt;
}
```

# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------


# Classes - Object Oriented - מונחה עצמים

### יצירת class:
```dart
class Dog {
  String _dogName;
  double _dogSpeed;

  Dog(String dogName, double dogSpeed) {
    _dogName = dogName;
    _dogSpeed = dogSpeed;
  }

  void voice() {
    print("$_dogName : wof wof");
  }

  void eat() {
    print("$_dogName : is eating now");
  }

  void run() {
    print("$_dogName : run at- $_dogSpeed kmh");
  }
}

/// כיצד להשתמש באובייקט שיצרנו
Dog dog1 = Dog("dog 1", 21.5);
Dog dog2 = Dog("dog 2", 10);

dog1.voice();
dog1.eat();
dog1.run();

dog2.voice();
dog2.eat();
dog2.run();
```


### ירושת מחלקות
```dart

//מחלקת אב - SuperClass
class Animal {
  String name;

  Animal(String name) {
    this.name = name;
  }

  void eat(){
    print("$name  is eatting now...");
  }
}

// מחלקות הבנים שיורשות ממחלקת האב  - SubClass
import 'animal.dart';
class Bird extends Animal {
  Bird(String birdName) : super(birdName) {}

  void fly(){
    print("im flying");
  }
}


class Dog extends Animal {
  Dog(String dogName) : super(dogName) {}

  void run(){
    print("im running");
  }
}



/// אופן השימוש

void main(){
  Bird bird1 = Bird("bird-1");
  Dog dog1 = Dog("dog-1");

  bird1.eat(); // >>> bird1 is eatting...
  bird1.fly(); // >>> im flying
  bird1.run(); // ERROR: no functions 'run' exists for Bird


  dog1.eat(); // >>> bird1 is eatting...
  dog1.fly(); // ERROR: no functions 'fly' exists for Dog
  dog1.run(); // >>> im running
}
```

### מחלקה אבסטרקטית - שיש לה פונקציה אחת לפחות ללא מימוש
```dart
/// מחלקת האב
abstract class Worker{
  Worker(){}

  // הפונקציה הזו היא אבסטרקטית כלומר כל מחלקת בן ממשת אותה בצורה אחרת
  void work();
}


/// מחלקות בנים
class Admin extends Worker(){
  Admin(){}

  // בגלל שהפונציה נמצאת במחלקה אבסטרקוטית אנחנו - חייבים לממש אותה!
  void work(){
    print('im the Admin i work very hard...');
  }
}

class Cleaner extends Worker(){
  Cleaner(){}
  
  void work(){
    print('im the Cleaner i want to clean the Windows');
  }
}
```


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------



# Data Structures - מבני נתונים


# רשימות - Lists
### יצירת רשימה
```dart
List<String> products = [
    "Milk", // 0
    "Choclate", // 1
    "Door", // 2
    "Ball", // 3
    "TV", // 4
  ];
```

### קבלת איבר מן הרשימה
```dart
products[0] // Milk
products[4] // TV
products[5] // ERROR(only 0-4 items)
```

### מעבר על רשימה והדפסתם
```dart
 for (int productNumber = 0;
      productNumber < products.length;
      productNumber++) {
    print("${products[productNumber]}");
  }
```

### הוספת איבר לרשימה
```dart
 products.add("iphone");
```

### מחיקת איבר מרשימה
```dart
 products.remove("TV");
```

# Map<Key, Value> - מיפוי בין מפתח לערך שלו
```dart
// String         : List<String>
// 'electoronics' : ['tv', 'laptop']
// 'food'         : ['choclate']
//
// Map<Key, Value> => {'key':'value'}

// Example 1: Map between Category to List of Products
Map<String, List<String>> category_listOfProducts_map = {
  'electoronics': ['tv', 'laptop'],
  'food': ['choclate'],
};
List<String> electronics = category_listOfProducts_map['electoronics']
print(electronics)
> ['tv', 'laptop']

// Example 2: Map between User to his Profile Details
Map<String, List<String>> userEmail_userDetails_map = {
  // email : [pass  , age ]
  'a@a.com': ['1234', '24'],
  'b@b.com': ['abcd', '24'],
};
List<String> userDetails = category_listOfProducts_map['b@b.com']
print(userDetails)
> ['abcd', '24']
```

# Queue - תור - לנהל סדר של אירועים\משימות\חפצים
```dart


/// Queue - תור - לנהל סדר של אירועים\משימות\חפצים
/// cleaner_missions = ['clean the dog cage', 'clean the birds cage']
/// custumers_queue = ['Avi', 'Sigal', 'Ronit']

import 'dart:collection';
Queue<String> custumers_queue = Queue<String>();
custumers_queue.add('avi');
custumers_queue.add('sigal');
custumers_queue.add('ronit');
print(custumers_queue.removeFirst()); // >>> avi
print(custumers_queue.removeFirst()); // >>> sigal
print(custumers_queue.removeFirst()); // >>> ronit
print(custumers_queue.removeFirst()); // Error: no more element in the Queue

custumers_queue.add('ronit');
custumers_queue.add('sigal');
print(custumers_queue.removeLast()); // >>> sigal
print(custumers_queue.removeLast()); // >>> ronit

```
# Set - קבוצה ללא כפילויות
```dart
import 'dart:collection';
/// Set - קבוצה ללא כפילויות

Set<String> customer_set = Set<String>();
customer_set.add('Avi 35489');
customer_set.add('Avi 35489');
customer_set.add('Roni 89754');
customer_set.add('Yossi 14545');
print(customer_set); // >>> {Avi 35489 , Roni 89754, Yossi 14545}
```