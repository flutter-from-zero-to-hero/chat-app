Full explanation is here:
https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet



# This is Title 1
## This is Title 2
### This is Title 3
#### This is Title 4
##### This is Title 5
###### This is Title 6

<br /> // this is new line
<br /> // this is new line

# This is the Story Title
**This is bold** this is not bold
## Part 1
בפרק הראשון כיפה אדומה הלכה
## Part 2
בפרק השני כיפה אדומה חזרה
### This is the menu:
* option 1
* option 2
  * option 2.1
  * option 2.2
    * option 2.2.1



# Todo List:
- [x] I need to wash the floor
- [ ] I need to wash the floor
- [x] I need to wash the floor


# Daily Routin
| A  |B   |C   | D  | E  |
|----|----|----|----|----|
| 1  | 2  |  3 |  4 |  5 |
| 1  | 2  |  3 |  4 |  5 |
| 1  | 2  |  3 |  4 |  5 |



# Text Styling
* bold: **bold**
* Italic: *Italic*

# Images and Videos
[![app development](https://mk0buildfireqbf86ll2.kinstacdn.com/wp-content/uploads/2017/10/become-mobile-app-developer.jpg)](http://www.youtube.com/watch?v=YOUTUBE_VIDEO_ID_HERE)
