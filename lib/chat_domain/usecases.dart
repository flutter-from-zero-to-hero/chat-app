import 'package:flutter_application_1/chat_domain/entities.dart';

class Usecases {
  Future sendMessage(
      Message msg, Function markAsSent, Function markAsFailed) async {
    print("sending the message");
    // TODO: You need to add your code here!
    if (msg.msg.length == 0) {
      await Future.delayed(Duration(seconds: 2));
      markAsFailed();
    } else {
      await Future.delayed(Duration(seconds: 2));
      markAsSent();
      messageReceived("HI Back");
      messageReceived(
          "HI Back---------------------------------------------------------------------------------------------------------------------------");
      messageReceived("HI Back 2");
    }
  }

  void messageReceived(String msg) {
    // TODO: you need to call to this function in the right place!
    if (msg.length <= 100) {
      onMessageReceived(msg);
    }
  }
}
