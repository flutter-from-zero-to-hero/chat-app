import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/chat_domain/usecases.dart';

enum SenderType {
  Me,
  Other,
  Title,
}

enum MessageStatus { Sending, Sent, FailedToSent }

void Function(String) onMessageReceived;

String currentUsername = "testuser";

class Message {
  String msg;
  MessageStatus sendMessageStatus;
  SenderType sender;
  Message({
    this.msg,
    this.sender,
  }) : this.sendMessageStatus = MessageStatus.Sending;
}

Usecases usecases = Usecases();
