import 'package:flutter/material.dart';
import 'package:flutter_application_1/presentation/chat_app.dart';

/*

- [v] FullStack - Client-Server
- [v] FullStack - Communication & Protocols
- [v] FullStack - ChatApp - Asyncronus Programming

- [v] FullStack - Client - ChatApp - Async Await & 3 layers - Part 1
- [v] FullStack - Client - ChatApp - Implement Logic - Part 2

- [v] FullStack - Server - Node.js - installation
- [v] FullStack - Server - Javascript - Types, functions - part 1
- [ ] FullStack - Server - Javascript - Objects, Array - part 2
- [ ] FullStack - API - Application Programming Interface

- [ ] FullStack - ChatApp - Real Server - Node.js - part 1
- [ ] FullStack - Socket IO - Application Platform Interface
- [ ] FullStack - ChatApp - Real Server - Node.js - part 2
- [ ] FullStack - ChatApp - Connect Client to Server

*/

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData.light(),
      home: ChatApp(),
    );
  }
}
